<?php

    use App\Http\Controllers\Authentification\RegisterController;
    use Illuminate\Support\Facades\Route;

    Route::get('/register', [RegisterController::class, 'showRegistrationForm'])->name('register');
    Route::post('/register', [RegisterController::class, 'register']);

?>