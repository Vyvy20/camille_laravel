<?php

    namespace database\Seeders;
    use Illuminate\Database\Seeder;
    use App\Models\Produit;
    use Illuminate\Support\Facades\DB;

    class ProduitSeeder extends Seeder 
    {
        public function run()
        {
            Produit::create([
                'titre' => 'Pokémon Méga Donjon Mystère',
                'description' => 'Vous êtes un humain téléporté pour d\'obscures" raisons dans le monde des Pokémon, vous en avez pris l\'aspect et vous pouvez communiquer avec eux.',
                'prix' => 64.99,
                'quantite' => 23,
            ]);
    
            Produit::create([
                'titre' => 'Miitopia',
                'description' => 'Miitopia est peuplé de Mii, ces avatars qui prennent notre place sur les consoles de Nintendo depuis l’arrivée de la Wii, en 2006. Les habitants de ce monde fantastique vivent en paix, jusqu’à ce qu’un grand méchant se mette à dérober leurs visages et à les apposer sur des monstres. Notre Mii est chargé de libérer les faciès des pauvres villageois en vainquant ces monstres.',
                'prix' => 29.99,
                'quantite' => 15,
            ]);

            Produit::create([
                'titre' => 'The Legend of Zelda : Breath of the Wild',
                'description' => 'The Legend of Zelda : Breath of the Wild est un jeu d\'action/aventure. Link se réveille d\'un sommeil de 100 ans dans un royaume d\'Hyrule dévasté. Il lui faudra percer les mystères du passé et vaincre Ganon, le fléau. L\'aventure prend place dans un gigantesque monde ouvert et accorde ainsi une part importante à l\'exploration. Le titre a été pensé pour que le joueur puisse aller où il veut dès le début, s\'éloignant de la linéarité habituelle de la série.',
                'prix' => 64.90,
                'quantite' => 24,
            ]);

            Produit::create([
                'titre' => 'Super smash bros ultimate',
                'description' => 'Super Smash Bros Ultimate est un jeu de combat sur Switch. Cet opus Ultimate regroupe l\'intégralité des combattants déjà apparus dans l\'histoire de Super Smash Bros. 75 personnages jouables seront disponibles en comptant le dresseur, ainsi que tous ses Pokémon.',
                'prix' => 54.99,
                'quantite' => 10,
            ]);

            Produit::create([
                'titre' => 'Overcooked 2',
                'description' => 'Overcooked 2 est un jeu d\'adresse développé par Team17 et Ghost Town Games. Tout comme son prédécesseur, le but est d\'être la cuisine la plus efficace possible à plusieurs. Le rush saura mettre à mal les amitiés les plus précieuses et les nerfs, à rude épreuve.',
                'prix' => 18.38,
                'quantite' => 9,
            ]);

            Produit::create([
                'titre' => 'Animal Crossing : New Horizons',
                'description' => 'Animal Crossing : New Horizons vous emmène de nouveau dans le monde mignon d\'Animal Crossing, sur Nintendo Switch. Cultivez votre potager, pêchez, et faites votre vie avec vos compagnons en temps réel grâce à l\'horloge de la console.',
                'prix' => 54.09,
                'quantite' => 27,
            ]);
        }
    }

?>