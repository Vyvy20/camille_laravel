<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    return new class extends Migration
    {
        /**
         * Run the migrations.
         */
        public function up(): void
        {
            Schema::create('utilisateur', function (Blueprint $table) {
                $table->id('idUtilisateur');
                $table->string('nom', 25);
                $table->string('prenom', 25);
                $table->string('email', 255)->unique();
                $table->string('mdp', 255);
                $table->enum('role', ['admin', 'client']);
            });
        }

        /**
         * Reverse the migrations.
         */
        public function down(): void
        {
            Schema::dropIfExists('utilisateur');
        }
    };
?>