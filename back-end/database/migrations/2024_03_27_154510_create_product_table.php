<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    return new class extends Migration
    {
        /**
         * Run the migrations.
         */
        public function up(): void
        {
            Schema::create('produit', function (Blueprint $table) {
                $table->id('idProduit');
                $table->string('titre', 100);
                $table->text('description')->nullable();
                $table->decimal('prix', 25, 2);
                $table->integer('quantite')->nullable();
            });
        }

        /**
         * Reverse the migrations.
         */
        public function down(): void
        {
            Schema::dropIfExists('produit');
        }
    };
?>