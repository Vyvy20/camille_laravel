<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    return new class extends Migration
    {
        /**
         * Run the migrations.
         */
        public function up(): void
        {
            Schema::create('commande', function (Blueprint $table) {

                $table->id('idCommande');
                $table->date('dateCommande')->nullable();
                $table->enum('statusCommande', ['en cours', 'expédiée', 'livrée', 'annulée']);
                $table->decimal('prixTotal', 25, 2);

                $table->unsignedBigInteger('idUtilisateur');
                $table->foreign('idUtilisateur')->references('idUtilisateur')->on('utilisateur')->onDelete('cascade');
            
            });
        }

        /**
         * Reverse the migrations.
         */
        public function down(): void
        {
            Schema::dropIfExists('order');
        }
    };
?>