<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    return new class extends Migration
    {
        /**
         * Run the migrations.
         */
        public function up(): void
        {
            Schema::create('produitcommande', function (Blueprint $table) {
                
                $table->primary(['idProduit', 'idCommande']);
    
                $table->unsignedBigInteger('idProduit');
                $table->foreign('idProduit')->references('idProduit')->on('produit')->onDelete('cascade');
    
                $table->unsignedBigInteger('idCommande');
                $table->foreign('idCommande')->references('idCommande')->on('commande')->onDelete('cascade');
    
                $table->integer('quantite');
            });
        }

        /**
         * Reverse the migrations.
         */
        public function down(): void
        {
            Schema::dropIfExists('product_order');
        }
    };
?>