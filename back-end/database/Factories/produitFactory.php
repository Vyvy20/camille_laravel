<?php

    namespace Database\Factories;

    use App\Models\Produit;
    use Illuminate\Database\Eloquent\Factories\Factory;

    class ProduitFactory extends Factory
    {
        /**
         *
         *
         * @var string
         */
        protected $model = Produit::class;

        public function definition()
        {
            return [];
        }
    }

?>