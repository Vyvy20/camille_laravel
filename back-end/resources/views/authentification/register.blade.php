<form method="POST" action="{{ route('register') }}">
    @csrf
    <input type="text" name="nom" placeholder="Nom" required>
    <input type="text" name="prenom" placeholder="Prénom" required>
    <input type="email" name="email" placeholder="Adresse email" required>
    <input type="password" name="mdp" placeholder="Mot de passe" required>
    <input type="password" name="mdp_confirmation" placeholder="Confirmer le mot de passe" required>
    <button type="submit">S'inscrire</button>
</form>