<?php

    namespace App\Http\Controllers\Authentification;

    use App\Http\Controllers\Controller;
    use App\Models\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;

    class RegisterController extends Controller
    {
        public function showRegistrationForm()
        {
            return view('authentification.register');
        }

        public function register(Request $request)
        {
            $request->validate([
                'nom' => 'required|string|max:25',
                'prenom' => 'required|string|max:25',
                'email' => 'required|string|email|unique:utilisateur|max:255',
                'mdp' => 'required|string|min:8|max:255|confirmed',
            ]);
            
            User::create([
                'nom' => $request->nom,
                'prenom' => $request->prenom,
                'email' => $request->email,
                'mdp' => Hash::make($request->mdp),
                'role' => 'client',
            ]);
            return redirect('/')->with('success', 'Inscription réussie !');
        }
    }

?>